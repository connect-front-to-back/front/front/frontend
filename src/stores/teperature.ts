import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/tempearatureService'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)

  const loadingStore = useLoadingStore()

  async function callConvert() {
    console.log('Stored: call Convert')
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
    console.log('Stored: Finish call Convert')
  }

  return {valid,result,celsius, callConvert}
})
