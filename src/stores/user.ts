import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/userService'
import type { User } from '@/types/User'

const loadingStore = useLoadingStore()
const users = ref<User[]>([])

export const useUserStore = defineStore('user', () => {
  const initilUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    roles: ['user']
  }
  const editedUser = ref<User>(JSON.parse(JSON.stringify(initilUser)))

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  async function saveUser() {
    const user = editedUser.value
    loadingStore.doLoad()
    if (!user.id) {
      //Add new
      const res = await userService.addUser(user)
    } else {
      //Update
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const res = await userService.delUser(editedUser.value)
    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initilUser))
  }
  return { users, getUsers, saveUser, deleteUser, clearForm, editedUser, getUser }
})
